# API-exercise - solution

# Pre-requisite
This setup is written for container, So we need k8s setup to run this code.
* [Minikube](https://kubernetes.io/docs/setup/minikube/)
* [Helm](https://github.com/helm/helm/releases)

## Structure
* [Dockerfile](./code/Dockerfile) : Dockerfile for python flask Vanilla image.
* [flask code](./code/app/conf/app.py) : config for python flask passed as configmap in helm.
* [Mysql Helm](./code/app/charts/mysql) : helm chart for Mysql defined as dependency for app flask.
* [mysql values](./code/app/charts/mysql/values.yaml) : where we define all config for mysql as parameter.
* [app values](./code/app/values.yaml) : where we define all config for application.

## Details
Here we are using Helm chart for setting up API with backedn DB MySql.
Once Helm package installed on k8s server.
## Run
* __helm init__  - this will setup tiller on k8s server which is used by helm client.
* __git clone https://gitlab.com/vickydev90/api-exercise.git__ -- clone repo on k8s/minikube
* __cd api-exercise/code__
* __helm install --name app app/__

this above command will setup API as well as Mysql DB in one go.
As We are using NodePort for API endpoint service, you will be able to access API on "http://host-ip:32765"
if NodePort doesn't works for you, then

* __kubectl get po__
* __kubectl port-forward app-76d8964dd5-2mglf 5000:5000__

here change name of app pod "app-76d8964dd5-2mglf" with actual name and you will be able to access API on "http://127.0.0.1:5000/people"

We have used __Configmap__ and __Secrets__ in our configuration for sharing the credentials inside containera and tried to __parameterized__ configuration as much as possible so we can re-use the code for any other application by changing in values file only.
