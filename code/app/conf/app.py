from flask import Flask, request, jsonify
from flaskext.mysql import MySQL
import os

app = Flask(__name__)
mysql = MySQL()

# MySQL configurations

app.config.from_envvar('creds')

DB_TABLE = os.environ.get("MYSQL_DATABASE_TABLE", default=None)

mysql.init_app(app)

@app.route('/people', methods = ['GET', 'POST'])
def get():
    if request.method == 'GET':
        cur = mysql.connect().cursor()
        cur.execute("select * from "+ DB_TABLE)
        r = [dict((cur.description[i][0], value)
                  for i, value in enumerate(row)) for row in cur.fetchall()]
        return jsonify({'myCollection' : r})

    if request.method == 'POST':
        data = request.get_json()
        conn = mysql.connect()
        cur = conn.cursor()
        cur.execute('''INSERT INTO titanic (`Survived`, `Pclass`, `Name`, `Sex`, `Age`, `Siblings/Spouses Aboard`, `Parents/Children Aboard`, `Fare`)
                   VALUES (%(Survived)s, %(Pclass)s, %(Name)s, %(Sex)s, %(Age)s, %(Siblings/Spouses Aboard)s, %(Parents/Children Aboard)s, %(Fare)s)''',
                   data)
        conn.commit()
        resp = jsonify('User added successfully!')
        resp.status_code = 200
        return resp

@app.route('/people/<uuid>', methods = ['GET', 'PUT', 'DELETE'])
def user(uuid):
    if request.method == 'GET':
        cur = mysql.connect().cursor()
        cur.execute("select * from %s WHERE uuid = %s" %(DB_TABLE, uuid))
        r = [dict((cur.description[i][0], value)
              for i, value in enumerate(row)) for row in cur.fetchall()]
        return jsonify({'myCollection' : r})

    if request.method == 'DELETE':
        conn = mysql.connect()
        cur = conn.cursor()
        cur.execute("DELETE FROM %s WHERE uuid = %s" %(DB_TABLE, uuid))
        conn.commit()
        return ("Record %s deleted successfully" %(uuid))

    if request.method == 'PUT':
        data = request.get_json()
        conn = mysql.connect()
        cur = conn.cursor()
        cur.execute('''UPDATE titanic SET Survived=%s, Pclass=%s, Name=%s, Sex=%s WHERE uuid = %s VALUES (%(Survived)s, %(Pclass)s, %(Name)s, %(Sex)s)''',
                   data, uuid)
        conn.commit()
        return ("Record %s updated successfully" %(uuid))

if __name__ == '__main__':
    app.debug = True
    app.run()
